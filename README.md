# README #

0. Clone this repo.
1. Checkout your own branch (i.e don't commit on master).
2. Make regular (local) commits.
3. Only push when you are complete (to your own branch).
4. Let us know when you are done.

Godspeed.
